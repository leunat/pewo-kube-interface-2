/* eslint-disable radix */
import React, { useState } from "react";
// eslint-disable-next-line import/no-extraneous-dependencies
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

const TheMutate = gql`
  mutation create_update($itemId: Int!, $body: String!) {
    create_update(item_id: $itemId, body: $body) {
      id
      body
      updatedAt: updated_at
      creatorId: creator_id
    }
  }
`;

// mutation {
//     create_update(item_id: 488793363, body: "add the update??") {
//       id
//     }
//   }

const Submit = id => {
  let disabled = false;
  const wrapperString = `<blockquote><b>geschrieben von ${id.newdata}:</b> <br/>`;

  const [value, setValue] = useState("");
  const [mutate, mutationProps] = useMutation(TheMutate, {
    onError: err => console.error(err)
  });

  const onSubmit = () => {
    disabled = true;
    mutate({
      variables: {
        itemId: parseInt(id.id),
        body: `${wrapperString + value}</blockquote>`
      }
    }).then(() => {
      disabled = false;

      // UPDATE here its HERE
    });
    setValue("");
  };

  return !disabled ? (
    <div>
      <form onSubmit={onSubmit}>
        {/* <textarea value={value} className="w-100" style={{height: "100px"}} onChange={onChange}></textarea> */}

        <CKEditor
          editor={ClassicEditor}
          config={{
            toolbar: [
              "bold",
              "italic",
              "link",
              "bulletedList",
              "numberedList",
              "blockQuote",
              "Indent",
              "Outdent"
            ]
          }}
          data=""
          onInit={() => {
            // You can store the "editor" and use when it is needed.
          }}
          onChange={(_event, editor) => {
            const data = editor.getData();
            setValue(data);
          }}
          onBlur={() => {}}
          onFocus={() => {}}
        />

        <div className="w-100">
          <button
            type="submit"
            value="Submit"
            className="btn btn-success float-right mt-2 mb-5"
          >
            absenden
          </button>
        </div>
      </form>
    </div>
  ) : null;
};

export default Submit;
