/* eslint-disable react/no-danger */
/* eslint-disable react/jsx-fragments */
/* eslint-disable radix */
/* eslint-disable react/destructuring-assignment */
import React, { Fragment } from "react";
import { useQuery } from "react-apollo";
import gql from "graphql-tag";
import Submit from "./Submit";

// const TheMutate = gql`
//   mutation  {
//     create_update (item_id: 20178755, body: "This update will be added to the item") {
//     id
//     }
//     }
// `;
const TheQuery = gql`
  query($id: Int!) {
    items(ids: [$id]) {
      updates {
        body
        created_at
        creator {
          name
        }
      }
      name
      id
      creator {
        name
      }
      column_values {
        id
        value
      }
    }
  }
`;

function Data(props) {
  const { loading, error, data } = useQuery(TheQuery, {
    variables: { id: parseInt(props.id) }
  });
  if (loading) return <p>Loading ...</p>;
  if (error) return window.location.reload(false);
  if (data.items[0] == null) return <p>no data</p>;

  return (
    <Fragment>
      <div>Langer Text</div>
      <div>
        {data.items[0].column_values[3].value
          ? data.items[0].column_values[3].value.substring(
              9,
              data.items[0].column_values[3].value.length - 42
            )
          : null}
      </div>
      <hr />
      <div>
        {data.items[0].updates.reverse().map(d => {
          //  return d.body.includes("<blockquote>") ? ( <Fragment>
          return d.body.includes("<blockquote>") ? (
            <Fragment>
              {d.body.includes("geschrieben von") ? (
                <div className="bg-light border border-secondary rounded p-3 mb-3 mr-5">
                  <div className="small text-right mb-3 text-secondary">
                    {data.items[0].column_values[0].value.substring(
                      1,
                      data.items[0].column_values[0].value.length - 1
                    )}{" "}
                    | {d.created_at}
                  </div>
                  <div
                    dangerouslySetInnerHTML={{ __html: d.body.split(":")[1] }}
                  />
                </div>
              ) : (
                <div className="bg-light border border-secondary rounded p-3 mb-3 ml-5">
                  <div className="small text-right mb-3 text-secondary">
                    {d.creator.name} | {d.created_at}
                  </div>
                  <div
                    dangerouslySetInnerHTML={{ __html: d.body.split(":")[0] }}
                  />
                </div>
              )}
            </Fragment>
          ) : null;
        })}
      </div>
      <Submit
        id={parseInt(props.id)}
        newdata={data.items[0].column_values[0].value.substring(
          1,
          data.items[0].column_values[0].value.length - 1
        )}
      />

      <div className="small text-right mb-3 text-secondary float-left">
        {data.items[0].column_values[0].value.substring(
          1,
          data.items[0].column_values[0].value.length - 1
        )}
      </div>
    </Fragment>
  );
}

// const data = id => (
//   // id.id
//   test(){
//   }
//   <DoTheQuerry fields={parseInt(id.id)} />
// );

export default Data;
