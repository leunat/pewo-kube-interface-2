import React from "react";
import { useQuery } from "react-apollo";
import gql from "graphql-tag";

// const TheMutate = gql`
//   mutation  {
//     create_update (item_id: 20178755, body: "This update will be added to the item") {
//     id
//     }
//     }
// `;
const TheQuery = gql`
  query items_by_column_values(
    $id: Int!
    $name: String!
    $column_value: String!
  ) {
    items_by_column_values(
      board_id: $id
      column_id: $name
      column_value: $column_value
    ) {
      id
      name

      board {
        state
        board_kind
      }
    }
  }
`;

function DoTheQuerry(props) {
  const { loading, error, data } = useQuery(TheQuery, {
    variables: { id: 488788273, name: "name", column_value: "betreff" }
  });
  if (loading) return <p> </p>;
  if (error) {
    return <p>data </p>;
  }

  //  props.parentCallback(data)

  // eslint-disable-next-line no-lone-blocks
  {
    props.props.parentCallback(data);
  }
  return <p />;
}

const data = props => (
  <>
    <DoTheQuerry props={props} />
  </>
);

export default data;