/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable radix */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
/* eslint-disable class-methods-use-this */
/* eslint-disable import/no-extraneous-dependencies */
import React from "react";
import gql from "graphql-tag";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { Mutation } from "@apollo/react-components";
import { Redirect } from "react-router";

const TheMutate = gql`
  mutation create_item(
    $board_id: Int!
    $group_id: String!
    $item_name: String!
    $value: JSON!
  ) {
    create_item(
      board_id: $board_id
      group_id: $group_id
      item_name: $item_name
      column_values: $value
    ) {
      id
      column_values {
        id
        value
      }
    }
  }
`;
const mutateAnsprechpartner = gql`
  mutation create_update($item_id: Int!, $body: String!) {
    create_update(item_id: $item_id, body: $body) {
      id
    }
  }
`;
// const [ mutate, mutationProps ]=useMutation(TheMutate, {onError: err => console.error(err)})
// const onSubmit = (event,data) => {
//     event.preventDefault()
// console.log(data);

// }

class Items extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Betreff: "",
      Ansprechpartner: "",
      Firma: "",
      Nachricht: "",
      Email: "",
      submit: false,
      item_id: ""
    };
    this.callbackFunction = this.callbackFunction.bind(this);
  }
  // onSubmit = (event) => {

  //     event.preventDefault();
  //       mutate({ variables: {board_id:488788273, group_id: "neue_gruppe", item_name : this.state.Betreff }}).then((response)=>{
  //         console.log(response);
  //         console.log("woho");

  //         // UPDATE here its HERE

  //       })

  //   }
  handleBetreffChange = event => {
    this.setState({
      Betreff: event.target.value
    });
  };

  handleAnsprechpartnerChange = event => {
    this.setState({
      Ansprechpartner: event.target.value
    });
  };

  handleEmailChange = event => {
    this.setState({
      Email: event.target.value
    });
  };

  handleFirmaChange = event => {
    this.setState({
      Firma: event.target.value
    });
  };

  callbackFunction() {}

  setNewStates(item) {
    this.setState({ submit: true, item_id: item });
  }

  render() {
    return this.state.submit ? (
      <>
        <Redirect to={this.state.item_id} />
      </>
    ) : (
      <>
        {/* <QueryGroupId parentCallback = {this.callbackFunction} /> */}
        <div className="container">
          <Mutation mutation={TheMutate}>
            {addNew => (
              <Mutation mutation={mutateAnsprechpartner}>
                {addNewComment => (
                  <form
                    onSubmit={e => {
                      e.preventDefault();
                      addNew({
                        variables: {
                          board_id: 488788273,
                          group_id: "neue_gruppe",
                          item_name: this.state.Betreff,
                          value: JSON.stringify({
                            text6: this.state.Ansprechpartner,
                            text9: this.state.Firma,
                            text: this.state.Email
                          })
                        }
                      }).then(response => {
                        addNewComment({
                          variables: {
                            item_id: parseInt(response.data.create_item.id),
                            body: `<blockquote>${this.state.Nachricht}</blockquote>`
                          }
                        }).then(
                          this.setState({
                            submit: true,
                            item_id: response.data.create_item.id
                          })
                        );
                      });

                      //    this.setState({submit:true});
                    }}
                    className="mt-4"
                  >
                    <div className="form-group">
                      <label htmlFor="exampleFormControlInput1">Betreff</label>
                      <input
                        required
                        type="textfield"
                        className="form-control"
                        id="exampleFormControlInput1"
                        placeholder="Betreff"
                        value={this.state.Betreff}
                        onChange={this.handleBetreffChange}
                      />
                    </div>
                    <div className="row">
                      <div className="col">
                        <div className="form-group">
                          <label htmlFor="exampleFormControlInput1">
                            Ansprechpartner
                          </label>
                          <input
                            required
                            type="textfield"
                            className="form-control"
                            id="exampleFormControlInput1"
                            placeholder="Ansprechpartner"
                            value={this.state.Ansprechpartner}
                            onChange={this.handleAnsprechpartnerChange}
                          />
                        </div>
                      </div>
                      <div className="col">
                        <div className="form-group">
                          <label htmlFor="exampleFormControlInput1">
                            Firma
                          </label>
                          <input
                            required
                            type="textfield"
                            className="form-control"
                            id="exampleFormControlInput1"
                            placeholder="Firma"
                            value={this.state.Firma}
                            onChange={this.handleFirmaChange}
                          />
                        </div>
                      </div>
                      <div className="col">
                        <div className="form-group">
                          <label htmlFor="exampleFormControlInput1">
                            Email
                          </label>
                          <input
                            required
                            type="email"
                            className="form-control"
                            id="exampleInputEmail1"
                            aria-describedby="emailHelp"
                            placeholder="Enter email"
                            value={this.state.Email}
                            onChange={this.handleEmailChange}
                          />
                        </div>
                      </div>
                    </div>

                    <div className="form-group">
                      <label htmlFor="exampleFormControlInput1">
                        Nachricht
                      </label>
                      <CKEditor
                        style={{ rows: "5" }}
                        editor={ClassicEditor}
                        config={{
                          toolbar: [
                            "bold",
                            "italic",
                            "link",
                            "bulletedList",
                            "numberedList",
                            "blockQuote",
                            "Indent",
                            "Outdent"
                          ]
                        }}
                        data={this.state.Nachricht}
                        onInit={() => {
                          // You can store the "editor" and use when it is needed.
                        }}
                        onChange={(event, editor) => {
                          const data = editor.getData();
                          this.setState({ Nachricht: data });
                        }}
                        onBlur={() => {}}
                        onFocus={() => {}}
                      />
                    </div>

                    <div className="form-group">
                      {/* <label htmlFor="exampleFormControlFile1">Datei</label>
                        <input type="file" className="form-control-file" id="exampleFormControlFile1"/> */}
                      <button
                        type="submit"
                        value="Submit"
                        className="btn btn-success float-right  mb-5"
                      >
                        {" "}
                        absenden
                      </button>
                    </div>
                  </form>
                )}
              </Mutation>
            )}
          </Mutation>
        </div>
      </>
    );
  }
}
export default Items;

// query {
//     items_by_column_values (board_id: 488788273, column_id: "name", column_value: "betreff") {
//     id
//     name

//     board {
//     state
//     board_kind
//     }
//     }
//     }

//     # query {
//     # boards (ids: 488788273) {
//     #   id
//     # owner {
//     # id
//     # }
//     # columns {
//     #   id

//     # title
//     # type
//     # }
//     # }
//     # }
