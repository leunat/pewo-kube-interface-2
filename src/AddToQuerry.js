import gql from "graphql-tag";
import { Mutation } from "@apollo/react-components";
import React, { Fragment, Component } from "react";
const ADD_TODO = gql`
  mutation($id: Int!, $Comments: String!) {
    create_update(item_id: $id, body: $Comments) {
      id
    }
  }
`;

const AddToQuerry = () => (
  <Query query={GET_TODOS}>
    {({ loading, error, data }) => {
      if (loading) return <p>Loading...</p>;
      if (error) return <p>Error :(</p>;

      return data.todos.map(({ id, type }) => {
        let input;

        return (
          <Mutation mutation={UPDATE_TODO} key={id}>
            {updateTodo => (
              <div>
                <p>{type}</p>
                <form
                  onSubmit={e => {
                    e.preventDefault();
                    updateTodo({ variables: { id, type: input.value } });

                    input.value = "";
                  }}
                >
                  <input
                    ref={node => {
                      input = node;
                    }}
                  />
                  <button type="submit">Update Todo</button>
                </form>
              </div>
            )}
          </Mutation>
        );
      });
    }}
  </Query>
);

export default AddToQuerry;
