import React from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Data from "./Data";
import Items from "./Items";

const url = window.location.href.split("/")[3];

const client = new ApolloClient({
  uri: "https://api.monday.com/v2/",
  headers: {
    "Content-Type": "application/json",
    Authorization:
      "eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjM5ODA3NTE3LCJ1aWQiOjEyMTg5Mzg3LCJpYWQiOiIyMDIwLTAzLTExIDA3OjI5OjMwIFVUQyIsInBlciI6Im1lOndyaXRlIn0.xBPqUyaZqMtCAnSM24NJHF9OVKXdNFpGzbdp_8gYIiQ"
  }
});

function App() {
  return (
    <Router>
      <ApolloProvider client={client}>
        <Switch>
          <Route path="/:id">
            <div className="container">
              <Data id={url} />
            </div>
          </Route>
          <Route path="/">
            <Items />
          </Route>
        </Switch>
      </ApolloProvider>
    </Router>
  );
}

export default App;
